<?php

namespace Drupal\trackmyadmin\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\trackmyadmin\QueryHelper;
use Drupal\Component\Utility\Xss;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Render\MetadataBubblingUrlGenerator;

/**
 * Reports Controller.
 */
class Reports extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The session.
   *
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
   */
  protected $session;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The currentpath.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The query helper.
   *
   * @var \Drupal\trackmyadmin\QueryHelper
   */
  protected $queryHelper;

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Render\MetadataBubblingUrlGenerator
   */
  protected $urlGenerator;

  /**
   * Constructs a new Reports instance.
   *
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The session.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   * @param \Drupal\trackmyadmin\QueryHelper $query_helper
   *   The query helper.
   * @param \Drupal\Core\Render\MetadataBubblingUrlGenerator $url_generator
   *   The url generator.
   */
  public function __construct(SessionInterface $session, MessengerInterface $messenger, CurrentPathStack $current_path, QueryHelper $query_helper, MetadataBubblingUrlGenerator $url_generator) {
    $this->session = $session;
    $this->messenger = $messenger;
    $this->currentPath = $current_path;
    $this->queryHelper = $query_helper;
    $this->urlGenerator = $url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('session'),
      $container->get('messenger'),
      $container->get('path.current'),
      $container->get('trackmyadmin.query_helper'),
      $container->get('url_generator')
    );
  }

  /**
   * Returns a render-able array for the Dashboard page.
   */
  public function dashboard() {
    $data = $this->queryHelper->fetchValues('content_config');
    $data['base_path'] = $this->urlGenerator->generateFromRoute('<front>', [], ['absolute' => TRUE]);

    return [
      // Your theme hook name.
      '#theme' => 'trackmyadmin_reports_theme',
      // Your variables.
      '#data' => $data,
    ];
  }

  /**
   * Returns a render-able array for the Name Report page.
   *
   * @param string $name
   *   The Identifier Name.
   */
  public function name($name) {
    $data = $this->queryHelper->fetchValues('name', ['data' => Xss::filter($name)]);
    $data['base_path'] = $this->urlGenerator->generateFromRoute('<front>', [], ['absolute' => TRUE]);
    $data['current_data'] = 'name';
    $data['data_item'] = Xss::filter($name);

    return [
      // Your theme hook name.
      '#theme' => 'trackmyadmin_reports_data_theme',
      // Your variables.
      '#data' => $data,
    ];
  }

  /**
   * Returns a render-able array for the User Report page.
   *
   * @param string $user
   *   The Identifier User.
   */
  public function user($user) {
    $user_details = user_load_by_name($user);
    $data = $this->queryHelper->fetchValues('uid', ['data' => $user_details->id()]);
    $data['base_path'] = $this->urlGenerator->generateFromRoute('<front>', [], ['absolute' => TRUE]);
    $data['current_data'] = 'user';
    $data['data_item'] = Xss::filter($user);

    return [
      // Your theme hook name.
      '#theme' => 'trackmyadmin_reports_data_theme',
      // Your variables.
      '#data' => $data,
    ];
  }

  /**
   * Returns a render-able array for the Team Report page.
   *
   * @param string $team
   *   The Identifier Team.
   */
  public function team($team) {
    $data = $this->queryHelper->fetchValues('team', ['data' => Xss::filter($team)]);
    $data['base_path'] = $this->urlGenerator->generateFromRoute('<front>', [], ['absolute' => TRUE]);
    $data['current_data'] = 'team';
    $data['data_item'] = Xss::filter($team);

    return [
      // Your theme hook name.
      '#theme' => 'trackmyadmin_reports_data_theme',
      // Your variables.
      '#data' => $data,
    ];
  }

  /**
   * Returns a render-able array for the Content Report page.
   */
  public function displayContent() {
    $data = $this->queryHelper->fetchValues('content');
    $data['base_path'] = $this->urlGenerator->generateFromRoute('<front>', [], ['absolute' => TRUE]);
    $data['current_data'] = 'Content';

    return [
      // Your theme hook name.
      '#theme' => 'trackmyadmin_reports_data_view_theme',
      // Your variables.
      '#data' => $data,
    ];
  }

  /**
   * Returns a render-able array for the Config Report page.
   */
  public function displayConfig() {
    $data = $this->queryHelper->fetchValues('config');
    $data['base_path'] = $this->urlGenerator->generateFromRoute('<front>', [], ['absolute' => TRUE]);
    $data['current_data'] = 'Config';

    return [
      // Your theme hook name.
      '#theme' => 'trackmyadmin_reports_data_view_theme',
      // Your variables.
      '#data' => $data,
    ];
  }

  /**
   * Returns a render-able array for the Data Flush Report page.
   */
  public function displayFlushData() {
    $data = $this->queryHelper->fetchValues('flush_data');
    $data['base_path'] = $this->urlGenerator->generateFromRoute('<front>', [], ['absolute' => TRUE]);
    $data['current_data'] = 'Data Flush';

    return [
      // Your theme hook name.
      '#theme' => 'trackmyadmin_reports_data_view_theme',
      // Your variables.
      '#data' => $data,
    ];
  }

  /**
   * Exports the data in CSV Format.
   */
  public function export() {
    $file_path = $this->queryHelper->downloadData();
    $this->messenger->addStatus($this->t('Download the CSV File <a href=":url">Here</a>.', [':url' => $file_path]));
    $route = $this->urlGenerator->generateFromRoute('<front>', [], ['absolute' => TRUE]) . '/admin/reports/trackmyadmin/settings/export';
    $response = new RedirectResponse($route);
    return $response->send();
  }

  /**
   * Flush the data in Dashboard.
   */
  public function flush() {
    // Let us make sure we download a copy before we flush the data.
    $file_path = $this->queryHelper->downloadData();
    // Initiate the flush.
    $this->queryHelper->deleteValues('trackmyadmin_activity');
    $tma_user_key = $this->session->get('tma_user_key');
    $values = [
      'id' => $tma_user_key,
      'field' => 'Data Flush',
      'value' => 'Data Flush',
      'url' => $this->currentPath->getPath(),
      'modified_type' => 'flush_data',
      'date' => time(),
    ];
    // Make an entry of the data flush happened.
    $this->queryHelper->insertValues('trackmyadmin_activity', $values);
    $this->messenger->addStatus($this->t('Data Flush Successful. Download the CSV File <a href=":url">Here</a>.', [':url' => $file_path]));
    $route = $this->urlGenerator->generateFromRoute('<front>', [], ['absolute' => TRUE]) . '/admin/reports/trackmyadmin/settings/flush';
    $response = new RedirectResponse($route);
    return $response->send();
  }

}
