<?php

namespace Drupal\trackmyadmin\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\trackmyadmin\QueryHelper;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * CaptureActivityDetails Controller.
 */
class CaptureActivityDetails extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The session.
   *
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
   */
  protected $session;

  /**
   * The query helper.
   *
   * @var \Drupal\trackmyadmin\QueryHelper
   */
  protected $queryHelper;

  /**
   * Constructs a new CaptureActivityDetails instance.
   *
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The session.
   * @param \Drupal\trackmyadmin\QueryHelper $query_helper
   *   The query helper.
   */
  public function __construct(SessionInterface $session, QueryHelper $query_helper) {
    $this->session = $session;
    $this->queryHelper = $query_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('session'),
      $container->get('trackmyadmin.query_helper')
    );
  }

  /**
   * Insert Captured Date into the {trackmyadmin_activity} table.
   */
  public function action(Request $request) {
    if ($this->session->has('tma_user_key')) {
      // Fetch the data values.
      $modifiedData = $request->get('details');
      if (!isset($modifiedData)) {
        return new Response();
      }
      elseif (!empty($modifiedData) && is_numeric($this->session->get('tma_user_key'))) {
        // Call query helper insert operation.
        foreach ($modifiedData as $data) {
          $data['id'] = $this->session->get('tma_user_key');
          $this->queryHelper->insertValues('trackmyadmin_activity', $data);
        }
      }
    }
    return new Response();
  }

}
