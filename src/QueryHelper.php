<?php

namespace Drupal\trackmyadmin;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Defines a Helper for all the queries.
 */
class QueryHelper {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The user entity storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * Constructs the QueryHelper Object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(Connection $connection, DateFormatter $date_formatter, FileSystemInterface $file_system, EntityTypeManagerInterface $entity_type_manager) {
    $this->connection = $connection;
    $this->dateFormatter = $date_formatter;
    $this->fileSystem = $file_system;
    $this->userStorage = $entity_type_manager->getStorage('user');
  }

  /**
   * Helper function to handle database insertions.
   *
   * @param string $table
   *   The name of the table in which data to be inserted.
   * @param array $values
   *   Array of values corresponding to the table.
   *
   * @return int
   *   Reference of the insertion.
   */
  public function insertValues($table, array $values) {
    $inserted = '';
    if ($table == 'trackmyadmin_users') {
      $inserted = $this->connection->insert($table)
        ->fields([
          'uid' => $values['uid'],
          'name' => $values['name'],
          'team' => $values['team'],
          'date' => $values['date'],
          'ip' => $values['ip'],
          'location' => $values['location'],
        ])
        ->execute();
    }
    if ($table == 'trackmyadmin_activity') {
      if (!isset($values['value'])) {
        $values['value'] = '';
      }
      $inserted = $this->connection->insert($table)
        ->fields([
          'id' => $values['id'],
          'field' => $values['field'],
          'url' => $values['url'],
          'value' => $values['value'],
          'modified_type' => $values['modified_type'],
          'date' => $values['date'],
        ])
        ->execute();
    }
    return $inserted;
  }

  /**
   * Helper function to handle database Fetch.
   *
   * @param string $type
   *   The type of the data as per need.
   * @param array $params
   *   Array of values corresponding to the type.
   *
   * @return array
   *   Array of data requested.
   */
  public function fetchValues($type, array $params = []) {
    $data = [];
    if ($type == 'content_config') {
      $content_results = $this->connection->query("SELECT COUNT(*) FROM {trackmyadmin_activity} WHERE modified_type LIKE 'content'")->fetchField();
      $config_results = $this->connection->query("SELECT COUNT(*) FROM {trackmyadmin_activity} WHERE modified_type LIKE 'config'")->fetchField();
      $flush_results = $this->connection->query("SELECT COUNT(*) FROM {trackmyadmin_activity} WHERE modified_type LIKE 'flush_data'")->fetchField();
      // Make sure no division by 0.
      if (($config_results + $content_results) > 0) {
        $data['config'] = round($config_results / ($config_results + $content_results), 2) * 100;
        $data['content'] = round($content_results / ($config_results + $content_results), 2) * 100;
      }
      else {
        $data['config'] = $data['content'] = 0;
      }
      if (!empty($flush_results)) {
        $data['flush'] = $flush_results;
      }
      $user_results = $this->connection->query("SELECT DISTINCT(id), date FROM {trackmyadmin_activity} ORDER BY date DESC")->fetchAll();
      if (!empty($user_results)) {
        foreach ($user_results as $user_result) {
          // Use this helper to fetch more details specific to user.
          $user_item = $this->recordsbyField('trackmyadmin_users', 'id', $user_result->id);
          if (!empty($user_item)) {
            $data['users'][$user_result->id]['name'] = $user_item[0]->name;
            $data['users'][$user_result->id]['team'] = $user_item[0]->team;
            $user = $this->userStorage->load($user_item[0]->uid);
            $data['users'][$user_result->id]['user'] = $user->label();
            if ($user_item[0]->date) {
              $data['users'][$user_result->id]['date'] = $this->dateFormatter->format($user_item[0]->date, 'custom', 'm-d-Y H:i:s');
            }
          }
        }
      }
    }
    if (($type == 'name' || $type == 'team' || $type == 'uid')  && isset($params['data'])) {
      $results = $this->recordsbyField('trackmyadmin_users', $type, $params['data']);
      if (!empty($results)) {
        foreach ($results as $key => $value) {
          $data['data'][$key]['name'] = $value->name;
          $data['data'][$key]['team'] = $value->team;
          $user = $this->userStorage->load($value->uid);
          $data['data'][$key]['user'] = $user->label();
          if ($value->date) {
            $data['data'][$key]['date'] = $this->dateFormatter->format($value->date, 'custom', 'm-d-Y H:i:s');
          }
          $data['data'][$key]['ip'] = $value->ip;
          $data['data'][$key]['location'] = $value->location;
        }
      }
    }
    if ($type == 'content' || $type == 'config' || $type == 'flush_data') {
      $results = $this->recordsbyField('trackmyadmin_activity', 'modified_type', $type);
      if (!empty($results)) {
        foreach ($results as $key => $value) {
          $user_item = $this->recordsbyField('trackmyadmin_users', 'id', $value->id);
          if (!empty($user_item)) {
            $data['data'][$key]['name'] = $user_item[0]->name;
            $data['data'][$key]['field'] = $value->field;
            $data['data'][$key]['url'] = $value->url;
            $data['data'][$key]['value'] = $value->value;
            if ($value->date) {
              $data['data'][$key]['date'] = $this->dateFormatter->format($value->date, 'custom', 'm-d-Y H:i:s');
            }
          }
        }
      }
    }
    if ($type == 'entire_data') {
      $data = $this->connection->query("SELECT Activity.field, Activity.url, Activity.value, Activity.date, Activity.modified_type, Users.uid, Users.name, Users.team, Users.date AS logged_in_date, Users.ip, Users.location FROM {trackmyadmin_activity} AS Activity INNER JOIN {trackmyadmin_users} AS Users ON Activity.id=Users.id")->fetchAll();
    }
    return $data;
  }

  /**
   * Helper function to handle database deletes.
   *
   * @param string $table
   *   The name of the table.
   *
   * @return int
   *   Reference of delete operation.
   */
  public function deleteValues($table) {
    return $this->connection->query("DELETE FROM {" . $table . "} WHERE modified_type NOT LIKE 'flush_data'");
  }

  /**
   * Helper function for database fetch.
   *
   * @param string $table
   *   The name of the table in which data to be fetched.
   * @param string $field
   *   The name of the field corresponding to the table above.
   * @param string $field_value
   *   The value corresponding to the field above.
   *
   * @return array
   *   Array of data requested.
   */
  public function recordsbyField($table, $field, $field_value) {
    return $this->connection->query("SELECT * FROM {" . $table . "} WHERE " . $field . " LIKE :field_value ORDER BY date DESC", [':field_value' => $field_value])->fetchAll();
  }

  /**
   * Helper function to download the CSV file.
   *
   * @return string
   *   Downloaded path of the CSV file.
   */
  public function downloadData() {
    $logged_in_date = $date = $file_path = NULL;
    $download_folder = 'public://trackmyadmin';
    // Create the directory if needed.
    if ($this->fileSystem->prepareDirectory($download_folder, FileSystemInterface::CREATE_DIRECTORY)) {
      $file_name = 'TrackMyAdminData_' . date('m-d-Y_H_i', time()) . '.csv';
      $file_path = \Drupal::service('file_url_generator')->generateAbsoluteString('public://trackmyadmin/' . $file_name);
      $fileObject = fopen($download_folder . '/' . $file_name, 'a');
      $data = $this->fetchValues('entire_data');
      if (!empty($data)) {
        $header_field = ['User Identified As',
          'From Team',
          'Logged in As',
          'Logged in On',
          'From Location',
          'Using IP',
          'Modified Field Name',
          'Modified Field Value',
          'Modified On Page',
          'Modified Type',
          'Modified On',
        ];
        // Insert the CSV Table headers.
        fputcsv($fileObject, $header_field);
        foreach ($data as $data_records) {
          $user = $this->userStorage->load($data_records->uid);
          if (is_numeric($data_records->logged_in_date)) {
            $logged_in_date = $this->dateFormatter->format($data_records->logged_in_date, 'custom', 'm-d-Y H:i:s');
          }
          if (is_numeric($data_records->date)) {
            $date = $this->dateFormatter->format($data_records->date, 'custom', 'm-d-Y H:i:s');
          }
          $fields = [$data_records->name,
            $data_records->team,
            $user->label(),
            $logged_in_date,
            $data_records->location,
            $data_records->ip,
            $data_records->field,
            $data_records->value,
            $data_records->url,
            $data_records->modified_type,
            $date,
          ];
          // The rest of the fields.
          fputcsv($fileObject, $fields);
        }
      }
    }
    return $file_path;
  }

}
