<?php

namespace Drupal\trackmyadmin\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Path\CurrentPathStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Drupal\Core\Render\MetadataBubblingUrlGenerator;

/**
 * Class Subscriber.
 */
class Subscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The session.
   *
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
   */
  protected $session;

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Render\MetadataBubblingUrlGenerator
   */
  protected $urlGenerator;

  /**
   * Constructs a new Subscriber instance.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The session.
   * @param \Drupal\Core\Render\MetadataBubblingUrlGenerator $url_generator
   *   The url generator.
   */
  public function __construct(AccountInterface $current_user, CurrentPathStack $current_path, SessionInterface $session, MetadataBubblingUrlGenerator $url_generator) {
    $this->currentUser = $current_user;
    $this->currentPath = $current_path;
    $this->session = $session;
    $this->urlGenerator = $url_generator;
  }

  /**
   * This is initiated only when KernelEvents::REQUEST.
   *
   * @see Symfony\Component\HttpKernel\KernelEvents
   */
  public function performRedirection(RequestEvent $event) {
    $trackmyadmin_form_path = '/trackmyadmin/capture_details';
    $roles = $this->currentUser->getRoles();
    // Let us force to provide the details in order to proceed any further.
    if (!$this->session->has('tma_user_key') && $this->currentPath->getPath() != $trackmyadmin_form_path && in_array('administrator', $roles)) {
      $route = $this->urlGenerator->generateFromRoute('<front>', [], ['absolute' => TRUE]) . $trackmyadmin_form_path;
      $event->setResponse(new RedirectResponse($route));
    }
    // Let us avoid the situation to collect details more than once.
    elseif ($this->session->has('tma_user_key') && is_numeric($this->session->get('tma_user_key')) && $this->currentPath->getPath() == $trackmyadmin_form_path && in_array('administrator', $roles)) {
      $route = $this->urlGenerator->generateFromRoute('<front>', [], ['absolute' => TRUE]) . 'admin';
      $event->setResponse(new RedirectResponse($route));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['performRedirection', 20];
    return $events;
  }

}
