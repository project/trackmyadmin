<?php

namespace Drupal\trackmyadmin\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\trackmyadmin\QueryHelper;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Component\Utility\Xss;

/**
 * Class CaptureDetailsForm.
 */
class CaptureDetailsForm extends FormBase {

  /**
   * The session.
   *
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
   */
  protected $session;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The query helper.
   *
   * @var \Drupal\trackmyadmin\QueryHelper
   */
  protected $queryHelper;

  /**
   * Constructs a new CaptureDetailsForm instance.
   *
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The session.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The Request Stack.
   * @param \Drupal\trackmyadmin\QueryHelper $query_helper
   *   The query helper.
   */
  public function __construct(SessionInterface $session, AccountProxy $current_user, RequestStack $request_stack, QueryHelper $query_helper) {
    $this->session = $session;
    $this->currentUser = $current_user;
    $this->requestStack = $request_stack->getCurrentRequest();
    $this->queryHelper = $query_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('session'),
      $container->get('current_user'),
      $container->get('request_stack'),
      $container->get('trackmyadmin.query_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tma_capturedetails_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $uname = $this->currentUser->getAccountName();
    $form['tma_name'] = [
      '#type' => 'textfield',
      '#size' => '60',
      '#placeholder' => 'Enter your Identity other than the "' . $uname . '" username',
      '#title' => $this->t('Your Name'),
      '#required' => TRUE,
      '#weight' => 1,
    ];
    $form['tma_team'] = [
      '#type' => 'select',
      '#title' => $this->t('Your Team'),
      '#options' => [
        'build' => $this->t('Build/Development'),
        'support' => $this->t('Support/Maintenance'),
        'business' => $this->t('Business'),
        'other' => $this->t('Other'),
      ],
      '#empty_option' => $this->t('- Select -'),
      '#weight' => 2,
    ];
    $form['tma_team_other'] = [
      '#type' => 'textfield',
      '#size' => '60',
      '#placeholder' => $this->t('Enter your Team Name'),
      '#weight' => 3,
      '#states' => [
      // Show textfield only if the above select field 'other' is selected.
        'visible' => [
          ':input[name="tma_team"]' => ['value' => 'other'],
        ],
      ],
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Proceed'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $tma_name = $form_state->getValue('tma_name');
    $tma_team = $form_state->getValue('tma_team');
    $tma_team_other = $form_state->getValue('tma_team_other');
    // Let us restrict users inputting strings 'admin', 'super', 'site'.
    if (preg_match('/admin/', $tma_name) || preg_match('/super/', $tma_name) || preg_match('/site/', $tma_name)) {
      $form_state->setErrorByName('tma_name', $this->t('Please avoid generic names such as admin etc'));
    }
    // Check whether either of the fields are inputted or not.
    if (empty($tma_team) && empty($tma_team_other)) {
      $form_state->setErrorByName('tma_team', $this->t('Please select a team'));
    }
    if (!empty($tma_team) && $tma_team == 'other' && empty($tma_team_other)) {
      $form_state->setErrorByName('tma_team_other', $this->t('Please input a team'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $tma_name = $form_state->getValue('tma_name');
    $tma_team = $form_state->getValue('tma_team');
    $tma_team_other = $form_state->getValue('tma_team_other');
    $uid = $this->currentUser->id();
    $location = NULL;
    $ip_address = $this->requestStack->getClientIp();
    // Location details from external source. Limit 120 requests per minute.
    $fetched_address = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $ip_address));
    if (isset($fetched_address['geoplugin_status']) && ($fetched_address['geoplugin_status'] >= 200 && $fetched_address['geoplugin_status'] <= 299)) {
      if ($fetched_address['geoplugin_city']) {
        $location = Xss::filter($fetched_address['geoplugin_city']) . ', ';
      }
      if ($fetched_address['geoplugin_regionName']) {
        $location = Xss::filter($fetched_address['geoplugin_regionName']) . ', ';
      }
      if ($fetched_address['geoplugin_countryName']) {
        $location = Xss::filter($fetched_address['geoplugin_countryName']);
      }
    }
    if (!empty($tma_team) && $tma_team == 'other' && !empty($tma_team_other)) {
      $tma_team = $form_state->getValue('tma_team_other');
    }
    $values = [
      'ip' => $ip_address,
      'location' => $location,
      'name' => $tma_name,
      'team' => $tma_team,
      'uid' => $uid,
      'date' => time(),
    ];
    // Call our query helper insert operation.
    $user_key = $this->queryHelper->insertValues('trackmyadmin_users', $values);
    $this->session->set('tma_user_key', $user_key);
    $form_state->setRedirect('system.admin');
  }

}
