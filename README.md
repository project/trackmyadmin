CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

TrackMyAdmin module tracks the activity of the modifications performed by the
Administrative Users. TrackMyAdmin module provides a dashboard that lists out
type(Content/Config) of modifications performed by the Administrative Users.

The TrackMyAdmin Dashboard provides in depth details about the modifications
performed by the user such as; field modified, value modified, url on which
this modification was performed, the date & time of this modification. This
Dashboard also provides details of the User who performed the modifications
such as the User Team, User Location, User IP Address and date & time when
logged in.

For Screenshots and more information about this module refer the drupal.org
page - https://www.drupal.org/project/trackmyadmin


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. You would now be prompted to Enter Your Identity(Other than the logged
       in username) and Your Team.
    3. Perform the modifications that you are intended to perform.
    4. Navigate to Administration > Reports > TrackMyAdmin Dashboard to access
       the list of modifications performed by various Administrative Users.


MAINTAINERS
-----------

Current maintainers:

 * John Paul Komaravalli (johnpk) - https://www.drupal.org/user/2308974
