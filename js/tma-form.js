/**
 * @file
 * Contains tma-form.js.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';
  $(function () {
    $(document).ready(function () {

      var modifiedFields = {};
      var $elementId = null;
      var $modified_type = null;
      var $elementValue = null;

      // Sniff all activity on the input elements.
      $('body').on('input', function (event) {
        var name = event.target.name;
        if (event.target.labels.length === 0) {
          if (event.target.placeholder !== "") {
            name = event.target.placeholder;
          }
        }
        else {
          name = event.target.labels[0].innerText;
        }
        modifiedFields[event.target.id] = name;
      });

      // Handling CKEDITOR activitiy in here. @TODO - decide going forward, shall we handle other widgets.
      if (drupalSettings.ckeditor) {
        CKEDITOR.on('instanceReady', function (event) {
          $(event.editor.container.$).find('iframe').contents().click(function (e) {
            modifiedFields[event.editor.name] = 'ckeditor';
          });
        });
      }

      $('input[type=submit]').on('click', function (e) {
        if (e.target.dataset.drupalSelector === 'edit-submit') {
          // Let us Traverse only if any Fields are Updated.
          if (modifiedFields.length !== 0) {
            var modifiedValues = [];
            var $url;
            $modified_type = 'content';
            var $location = $(location).attr('href');
            // @TODO - find any better way to distinguish content vs config.
            if ($(location).attr('href').match(/\/admin\/config/)) {
              $modified_type = 'config';
            }
            $.each(modifiedFields, function (arrKey, arrValue) {
              if (arrValue === 'ckeditor') {
                $elementValue = CKEDITOR.instances[arrKey].getData();
                arrValue = arrKey;
              }
              else {
                $elementId = '#' + arrKey;
                $elementValue = $($elementId).val();
              }
              if ($elementValue) {
                $elementValue = $elementValue.replace(/<[^>]+>/g, '');
              }
              // Make sure to Strip possible tags.
              modifiedValues.push({field: arrValue, value: $elementValue, url: $location.replace(/<[^>]+>/g, ''), modified_type: $modified_type, date: Math.floor(Date.now() / 1000)});
            });
            if (drupalSettings.path.baseUrl !== '/') {
              $url = drupalSettings.path.baseUrl + 'trackmyadmin/capture_activity_details';
            }
            else {
              $url = '/trackmyadmin/capture_activity_details';
            }
            $.ajax({method: 'POST', url: $url, data: {details: modifiedValues}, success: function (data) {}});
          }
        }
      });

    });
  });
})(jQuery, Drupal, drupalSettings);
